using CoreLib.InternalApi.Pipelines;
using CoreLib.PublicApi;

namespace CoreLib.InternalApi.CommonFlows;

// Comentari: jo normalment no empraria herència, m'agrada més composició de classes, però si feim aquesta part des framework "para tontos", és una eina útil per simplificar
public abstract class PipelinedSearchConnector : ISearchConnector
{
    private IPipeline<SearchRequest, IAsyncEnumerable<Rate>> pipeline;
    
    // Ineficient: construim un nou pipeline cada vegada que cream una instància del connector
    // A una implementació anterior feia es pipeline static per evitar-ho, però podia dur a errors d'implementació
    // Hi ha solucions a aquest problema, però no són simples
    public PipelinedSearchConnector() => 
        pipeline = BuildPipeline();

    public IAsyncEnumerable<Rate> SearchAsync(SearchRequest request) => 
        pipeline.Execute(request);

    protected abstract IPipeline<SearchRequest, IAsyncEnumerable<Rate>> BuildPipeline();
}