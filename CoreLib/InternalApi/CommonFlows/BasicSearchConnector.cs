using CoreLib.PublicApi;

namespace CoreLib.InternalApi.CommonFlows;

// Comentari: jo normalment no empraria herència, m'agrada més composició de classes, però si feim aquesta part des framework "para tontos", és una eina útil per simplificar
public abstract class BasicSearchConnector<TProviderRequest, TProviderResponse> : ISearchConnector
{
    public async IAsyncEnumerable<Rate> SearchAsync(SearchRequest request)
    {
        if (!IsRequestValid(request)) {
            // TO DO: que feim en aquest cas?
            yield break;
        }

        var providerRequest = MapRequest(request);

        var providerResponse = await CallProviderAsync(providerRequest);

        await foreach(var rate in MapResponseAsync(providerResponse)) 
        {
            yield return rate;
        }
    }

    protected abstract bool IsRequestValid(SearchRequest request);
    protected abstract TProviderRequest MapRequest(SearchRequest request);
    protected abstract Task<TProviderResponse> CallProviderAsync(TProviderRequest request);
    protected abstract IAsyncEnumerable<Rate> MapResponseAsync(TProviderResponse response);
}