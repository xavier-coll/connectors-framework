using CoreLib.PublicApi;

namespace CoreLib.InternalApi;

public interface ISearchConnector
{
    // Utilitzam directament sa SearchRequest pública perquè no he previst que s'hagi de fer cap transformació
    // Retornam IAsyncEnumerable per permetre que s'implementació faixi stream de dades (si no es fa tampoc passa res, ja és un tema de qualitat d'implementació)
    IAsyncEnumerable<Rate> SearchAsync(SearchRequest request);
}