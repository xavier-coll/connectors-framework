namespace CoreLib.InternalApi.Pipelines;

public static class PipelineBuilder
{
    public static PipelineBuilder<TRequest, TResponse> Start<TRequest, TResponse>(IPipeline<TRequest, TResponse> next) =>
        new(next);
        
    public static PipelineBuilder<TRequest, TResponse> Start<TRequest, TResponse>(Func<TRequest, TResponse> next)
    {
        var wrappedNext = new Adapters.Fn<TRequest, TResponse>(next);

        return Start(wrappedNext);
    }
}

public class PipelineBuilder<TRequest, TResponse>
{
    private readonly IPipeline<TRequest, TResponse> pipeline;

    public PipelineBuilder(IPipeline<TRequest, TResponse> pipeline) =>
        this.pipeline = pipeline;

    public IPipeline<TRequest, TResponse> Build() => pipeline;

    public PipelineBuilder<TRequest, TNewResponse> Next<TNewResponse>(IPipeline<TResponse, TNewResponse> next)
    {
        var composed = new Adapters.Composed<TRequest, TResponse, TNewResponse>(
            pipeline, 
            next
        );

        return new(composed);
    }

    public PipelineBuilder<TRequest, TNewResponse> Next<TNewResponse>(Func<TResponse, TNewResponse> next)
    {
        var wrappedNext = new Adapters.Fn<TResponse, TNewResponse>(next);

        return Next(wrappedNext);
    }
}

public static class PipelineBuilderExtensions
{
    public static PipelineBuilder<TRequest, Task<TNewResponse>> Next<TRequest, TResponse, TNewResponse>(
        this PipelineBuilder<TRequest, Task<TResponse>> me,
        IPipeline<TResponse, TNewResponse> next)
    {
        var wrappedNext = new Adapters.AsyncRequestOnSyncPipeline<TResponse, TNewResponse>(
            next
        );

        return me.Next(wrappedNext);
    }
    
    public static PipelineBuilder<TRequest, Task<TNewResponse>> Next<TRequest, TResponse, TNewResponse>(
        this PipelineBuilder<TRequest, Task<TResponse>> me,
        Func<TResponse, TNewResponse> next)
    {
        var wrappedNext = new Adapters.Fn<TResponse, TNewResponse>(next);

        return me.Next(wrappedNext);
    }

    public static PipelineBuilder<TRequest, Task<TNewResponse>> Next<TRequest, TResponse, TNewResponse>(
        this PipelineBuilder<TRequest, Task<TResponse>> me,
        IPipeline<TResponse, Task<TNewResponse>> next) 
    {
        var wrappedNext = new Adapters.AsyncRequestOnAsyncPipeline<TResponse, TNewResponse>(
            next
        );

        return me.Next(wrappedNext);
    }

    public static PipelineBuilder<TRequest, Task<TNewResponse>> Next<TRequest, TResponse, TNewResponse>(
        this PipelineBuilder<TRequest, Task<TResponse>> me,
        Func<TResponse, Task<TNewResponse>> next) 
    {
        var wrappedNext = new Adapters.Fn<TResponse, Task<TNewResponse>>(next);

        return me.Next(wrappedNext);
    }

    public static PipelineBuilder<TRequest, IAsyncEnumerable<TNewResponseItem>> Next<TRequest, TResponse, TNewResponseItem>(
        this PipelineBuilder<TRequest, Task<TResponse>> me,
        IPipeline<TResponse, IAsyncEnumerable<TNewResponseItem>> next) 
    {
        var wrappedNext = new Adapters.AsyncRequestOnAsyncEnumerablePipeline<TResponse, TNewResponseItem>(
            next
        );

        return me.Next(wrappedNext);
    }

    public static PipelineBuilder<TRequest, IAsyncEnumerable<TNewResponseItem>> Next<TRequest, TResponse, TNewResponseItem>(
        this PipelineBuilder<TRequest, Task<TResponse>> me,
        Func<TResponse, IAsyncEnumerable<TNewResponseItem>> next) 
    {
        var wrappedNext = new Adapters.Fn<TResponse, IAsyncEnumerable<TNewResponseItem>>(next);

        return me.Next(wrappedNext);
    }
}

file static class Adapters
{
    public class Fn<TRequest, TResponse> : IPipeline<TRequest, TResponse>
    {
        private readonly Func<TRequest, TResponse> func;

        public Fn(Func<TRequest, TResponse> func) =>
            this.func = func;

        public TResponse Execute(TRequest request) => func(request);
    }

    public class Composed<TRequest, TResponsePrev, TResponse> : IPipeline<TRequest, TResponse>
    {
        private readonly IPipeline<TRequest, TResponsePrev> pipeline1;
        private readonly IPipeline<TResponsePrev, TResponse> pipeline2;

        public Composed(
            IPipeline<TRequest, TResponsePrev> pipeline1, 
            IPipeline<TResponsePrev, TResponse> pipeline2)
        {
            this.pipeline1 = pipeline1;
            this.pipeline2 = pipeline2;
        }

        public TResponse Execute(TRequest request) =>
            pipeline2.Execute(pipeline1.Execute(request));
    }

    public class AsyncRequestOnSyncPipeline<TRequest, TResponse> : IPipeline<Task<TRequest>, Task<TResponse>>
    {
        private readonly IPipeline<TRequest, TResponse> pipeline;

        public AsyncRequestOnSyncPipeline(IPipeline<TRequest, TResponse> pipeline) =>
            this.pipeline = pipeline;

        public async Task<TResponse> Execute(Task<TRequest> request) =>
            pipeline.Execute(await request);
    }

    public class AsyncRequestOnAsyncPipeline<TRequest, TResponse> : IPipeline<Task<TRequest>, Task<TResponse>>
    {
        private readonly IPipeline<TRequest, Task<TResponse>> pipeline;

        public AsyncRequestOnAsyncPipeline(IPipeline<TRequest, Task<TResponse>> pipeline) =>
            this.pipeline = pipeline;

        public async Task<TResponse> Execute(Task<TRequest> request) =>
            await pipeline.Execute(await request);
    }
    
    public class AsyncRequestOnAsyncEnumerablePipeline<TRequest, TResponseItem> : IPipeline<Task<TRequest>, IAsyncEnumerable<TResponseItem>>
    {
        private readonly IPipeline<TRequest, IAsyncEnumerable<TResponseItem>> pipeline;

        public AsyncRequestOnAsyncEnumerablePipeline(IPipeline<TRequest, IAsyncEnumerable<TResponseItem>> pipeline) =>
            this.pipeline = pipeline;

        public async IAsyncEnumerable<TResponseItem> Execute(Task<TRequest> request)
        {
            await foreach(var item in pipeline.Execute(await request))
            {
                yield return item;
            }
        }
    }
}