namespace CoreLib.InternalApi.Pipelines;

public interface IPipeline<TRequest, TResponse>
{
    TResponse Execute(TRequest request);
}