using CoreLib.InternalApi;
using CoreLib.PublicApi;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace CoreLib.AspNet;

public static class Connector
{
    public static void RunSearchApi<TConnector>(string[] args, Action<IServiceCollection>? addDependencies = null)
        where TConnector : class, ISearchConnector
    {
        var builder = WebApplication.CreateBuilder(args);

        // --- ADD DEPENDENCIES ---
        //// Add Swager
        builder.Services.AddEndpointsApiExplorer();
        builder.Services.AddSwaggerGen();

        // Configure a default HttpClient (no he comprovat que estigui bé, pot contenir errors, és només per mostrar sa idea)
        // He triat Transient tenint en compte lo que posa aquest article: https://learn.microsoft.com/en-us/dotnet/architecture/microservices/implement-resilient-applications/use-httpclientfactory-to-implement-resilient-http-requests
        builder.Services.AddHttpClient("default", client => 
        {
            // TO DO: general configuration
        });
        builder.Services.AddTransient<HttpClient>(serviceProvider => 
            serviceProvider.GetRequiredService<IHttpClientFactory>().CreateClient("default")
        );

        //// Add the search connector and its dependencies
        builder.Services.AddTransient<SearchConnector>();
        builder.Services.AddTransient<ISearchConnector, TConnector>();
        addDependencies?.Invoke(builder.Services);


        // --- BUILD THE APP REQUEST PIPELINE  ---
        var app = builder.Build();

        // Configure the HTTP request pipeline.
        if (app.Environment.IsDevelopment())
        {
            app.UseSwagger();
            app.UseSwaggerUI();
        }

        app.UseHttpsRedirection();

        app.MapPost("/search", (SearchRequest request, SearchConnector connector) => 
            connector.SearchAsync(request)
        );

        app.Run();
    }
}