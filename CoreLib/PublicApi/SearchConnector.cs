using IInternalSearchConnector = CoreLib.InternalApi.ISearchConnector;

namespace CoreLib.PublicApi;

// Sa idea de separar es SearchConnector de s'internalConnector és poder fer tasques comuns a nivell de framework (per ex. mapejar codis o transformar-ho a un format més eficient a nivell de tamany de missatge)
// Se podria crear una interface per es SearchConnector public, però realment és innecessari (a no ser que sigui necessari a nivell de testing)
public class SearchConnector
{
    private IInternalSearchConnector internalConnector;

    public SearchConnector(IInternalSearchConnector internalConnector) => 
        this.internalConnector = internalConnector;

    // Una alternativa podria ser també tornar un IAsyncEnumerable a aquest nivell per permetre streaming de dades a tots es nivells
    public async Task<SearchResponse> SearchAsync(SearchRequest request)
    {
        await foreach (var rate in internalConnector.SearchAsync(request))
        {
        }

        return new();
    }
}