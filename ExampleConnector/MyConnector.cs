using System.Runtime.CompilerServices;
using CoreLib.InternalApi;
using CoreLib.InternalApi.CommonFlows;
using CoreLib.InternalApi.Pipelines;
using CoreLib.PublicApi;

namespace ExampleConnector;

public static class MyConnector
{
    // Option 1: implement the basic connector and use an HttpClient
    public class V1 : BasicSearchConnector<Provider.Search1Request, Provider.Search1Response>
    {
        private readonly HttpClient client;
        private readonly ILogger logger;

        public V1(HttpClient client, ILogger<V2> logger)
        {
            this.client = client;
            this.logger = logger;
        }

        protected override bool IsRequestValid(SearchRequest request)
        {
            LogMethodIsExecuted(logger);
            
            return true;
        }

        protected override Provider.Search1Request MapRequest(SearchRequest request)
        {
            LogMethodIsExecuted(logger);

            return new();
        }

        protected override async Task<Provider.Search1Response> CallProviderAsync(Provider.Search1Request request)
        {
            await Task.Delay(50);

            LogMethodIsExecuted(logger);

            return new();
        }

        protected override async IAsyncEnumerable<Rate> MapResponseAsync(Provider.Search1Response response)
        {
            await Task.Delay(50);

            LogMethodIsExecuted(logger);
            
            foreach(var _ in Enumerable.Range(1, 3))
            {
                yield return new();
            }
        }
    }

    // Option 2: implement the Option 1 using the PipelinedSearchConnector (limitació: ara mateix no es pot "cortocircuitar" es pipeline)
    public class V2 : PipelinedSearchConnector
    {
        private readonly ILogger logger;

        public V2(ILogger<V2> logger)
        {
            this.logger = logger;
        }

        protected override IPipeline<SearchRequest, IAsyncEnumerable<Rate>> BuildPipeline() =>
            PipelineBuilder
                .Start((SearchRequest rq) => (request1: MapRequest1(rq), request: rq))
                .Next(async param => 
                    (response1: await CallProvider1Async(param.request1), param.request)
                )
                .Next(param => MapRequest2(param.response1, param.request))
                .Next(CallProvider2Async)
                .Next(MapResponse2Async)
                .Build();

        private Provider.Search1Request MapRequest1(SearchRequest request)
        {
            LogMethodIsExecuted(logger);

            return new();
        }

        private async Task<Provider.Search1Response> CallProvider1Async(Provider.Search1Request request)
        {
            await Task.Delay(50);

            LogMethodIsExecuted(logger);

            return new();
        }

        private Provider.Search2Request MapRequest2(Provider.Search1Response response1, SearchRequest request)
        {
            LogMethodIsExecuted(logger);

            return new();
        }

        private async Task<Provider.Search2Response> CallProvider2Async(Provider.Search2Request request)
        {
            await Task.Delay(50);

            LogMethodIsExecuted(logger);

            return new();
        }

        private async IAsyncEnumerable<Rate> MapResponse2Async(Provider.Search2Response response)
        {
            await Task.Delay(50);

            LogMethodIsExecuted(logger);
            
            foreach(var _ in Enumerable.Range(1, 3))
            {
                yield return new();
            }
        }
    }
    
    // Option 3: implement the interface directly, optionally you can add some dependencies to keep this class clean and simple
    public class V3 : ISearchConnector
    {
        private readonly ILogger logger;

        public V3(ILogger<V3> logger) => 
            this.logger = logger;

        public async IAsyncEnumerable<Rate> SearchAsync(SearchRequest request)
        {
            // Validate request    
            if (!IsRequestValid(request)) {
                // TO DO: que feim en aquest cas?
                yield break;
            }

            // Call provider's first method
            var request1 = MapRequest1(request);

            var response1 = await CallProvider1Async(request1);

            // Call provider's second method
            var request2 = MapRequest2(response1, request);

            var response2 = await CallProvider2Async(request2);

            // Build and return response
            await foreach (var rate in MapResponse2Async(response2))
            {
                yield return rate;
            }
        }

        private bool IsRequestValid(SearchRequest request)
        {
            LogMethodIsExecuted(logger);
            
            return true;
        }

        private Provider.Search1Request MapRequest1(SearchRequest request)
        {
            LogMethodIsExecuted(logger);

            return new();
        }

        private async Task<Provider.Search1Response> CallProvider1Async(Provider.Search1Request request)
        {
            await Task.Delay(50);

            LogMethodIsExecuted(logger);

            return new();
        }

        private Provider.Search2Request MapRequest2(Provider.Search1Response response1, SearchRequest request)
        {
            LogMethodIsExecuted(logger);

            return new();
        }

        private async Task<Provider.Search2Response> CallProvider2Async(Provider.Search2Request request)
        {
            await Task.Delay(50);

            LogMethodIsExecuted(logger);

            return new();
        }

        private async IAsyncEnumerable<Rate> MapResponse2Async(Provider.Search2Response response)
        {
            await Task.Delay(50);

            LogMethodIsExecuted(logger);
            
            foreach(var _ in Enumerable.Range(1, 3))
            {
                yield return new();
            }
        }
    }

    // Helpers
    private static void LogMethodIsExecuted(ILogger logger, [CallerMemberName] string callerName = "") => 
        logger.LogInformation($"{callerName} method executed");

}